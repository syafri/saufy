<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMangaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manga', function (Blueprint $table) {
            $table->smallIncrements('id_manga');
            $table->string('nama_manga');
            $table->string('slug_manga');
            $table->string('gambar_manga');
            $table->smallInteger('release')->nullable();;
            $table->bigInteger('views')->nullable();;
            $table->text('summary')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manga');
    }
}
