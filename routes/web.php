<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index')->name('home');
Route::get('/manga', 'UserController@mangaList');
Route::get('/manga/{slug}', 'UserController@chapterList');

Auth::routes();

Route::get('/admin/dashboard', 'admin\AdminController@index')->name('admin');
Route::get('/admin/logout', 'admin\AdminController@logout');


Route::get('/admin/manage-manga', 'admin\AnimeController@index')->name('manage-manga');