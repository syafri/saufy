@extends('layouts.layout')
@section('konten')

    <?php $no = 1; ?>
    @foreach($mangas as $manga)
        @if($no == 1 || $no == 5 || $no == 9)
            <div style="margin-bottom: 10px;">
        @endif
                <div style="float: left; <?php echo ($no % 4 == 0 ? 'margin-right: 0 px;' : 'margin-right: 12px;') ?>  width: 156px;">
                    <div><img src="img/img1.jpg" style="width:156px; height:200px; margin-bottom: 5px;"></div>
                    <div style="text-align: center;"> <span style="color: white;"> <?php echo $manga->nama_manga; ?> </span> </div>
                </div>
        @if($no % 4 == 0)
                <div class="clearfix"></div>     
            </div>
        @endif
         <?php $no = $no + 1; ?>
    @endforeach

    <div class="row" style="margin-top: 20px;">
        <div class="col">
            <nav aria-label="Page navigation example">
                {{ $mangas->links() }}
            </nav>
        </div>
    </div>
@stop