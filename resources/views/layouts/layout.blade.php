<!DOCTYPE html>
<html>
    <head>
        <title> x hunter</title>
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="{{ asset('/js/app.js') }}" ></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='custom-css-css'  href='https://nanime.tv/template/nontonanime.org/css/custom.css?ver=0.17' type='text/css' media='all' />
        <link rel='stylesheet' id='owl'  href="{{ asset('/css/slider/owl.carousel.css') }}" type='text/css' media='all' />
    </head>
    <body>
        <div class="row">
            <div class="col">
                <img src="img/x.png" width="440px" style="margin-left:135px; margin-bottom: -17px;">
            </div>
        </div>
        <div class="container" style="background-color:#0b071a;">

            <!-- NAVBAR -->
            <div class="row">
                <div class="col" style="padding:0; margin-bottom: 10px;">
                    <nav class="navbar navbar-expand-lg" style="background-color: #8f0808; border-bottom: 5px solid #551b1b; border-radius: 5px;">
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="line-height: 0;">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/manga') }}">Daftar Komik</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Update Terbaru</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Komik Tamat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Tahun</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Genre</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Jadwal Rilis</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact Us</a>
                                </li>
                            </ul>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            </form>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- TUTUP NAVBAR -->

            <!-- SLIDER -->
            <header style="margin-bottom: 10px;">
                <div class="row">
                    <div id="slider-utama" class="header-image owl-carousel">
                        <div class="item slider-item">
                            <a href="/anime/black-clover">
                                <img style="" class="img-responsive shadow" title="Nonton anime: Black Clover" src="https://image.tmdb.org/t/p/w154/kaMisKeOoTBPxPkbC3OW7Wgt6ON.jpg">
                            </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/ore-wo-suki-nano-wa-omae-dake-ka-yo">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Ore wo Suki nano wa Omae dake ka yo" src="https://image.tmdb.org/t/p/w154/4MojZik5N62IShd2BFVEHyaRBLP.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/shinchou-yuusha-kono-yuusha-ga-ore-tueee-kuse-ni-shinchou-sugiru">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Shinchou Yuusha: Kono Yuusha ga Ore Tueee Kuse ni Shinchou Sugiru" src="https://image.tmdb.org/t/p/w154/9fM6EF5U1BbWrwgXkfRWPZ3V1hK.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/vinland-saga">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Vinland Saga" src="https://image.tmdb.org/t/p/w154/vNr7BQHryOfnxXztN7UNy0im9sg.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/detective-conan">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Detective Conan" src="https://img.topddl.net/images/eeb879c1ea047d0f3e8e56815fa5894a.th.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/movie/doraemon-movie-39-nobita-no-getsumen-tansaki">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Doraemon Movie 39: Nobita no Getsumen Tansaki" src="https://image.tmdb.org/t/p/w154/aoPHoBEjEl7Axkt7UaJnlqPmpL.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/arifureta-shokugyou-de-sekai-saikyou">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Arifureta Shokugyou de Sekai Saikyou" src="https://image.tmdb.org/t/p/w154/10C7OzFgnqBjI17bNm1nlLSOEBH.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/ahiru-no-sora">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Ahiru no Sora" src="https://image.tmdb.org/t/p/w154/n0M1aeqSP8NJ7YQ76sWQw3EURq8.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/dr-stone">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Dr. Stone" src="https://image.tmdb.org/t/p/w154/64DIVpWiGlvvJczMnyu8jojSYKm.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/isekai-cheat-magician">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Isekai Cheat Magician" src="https://image.tmdb.org/t/p/w154/1g7KSwN9yDBWX6bdjCrz3zmaA0t.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/boruto-naruto-next-generations">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Boruto: Naruto Next Generations" src="https://image.tmdb.org/t/p/w154/xnYuyTdvLx9p055rKiYM2qMm9hg.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/dungeon-ni-deai-wo-motomeru-no-wa-machigatteiru-darou-ka-ii">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka II" src="https://img.topddl.net/images/c8bf0f77521f943bac2c2237dfda4111.th.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/fire-force">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Fire Force" src="https://image.tmdb.org/t/p/w154/xwKGTFXL2kKz6P0WI23Q2ecaGOO.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/kimetsu-no-yaiba">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Kimetsu no Yaiba" src="https://image.tmdb.org/t/p/w154/xDvsHZ0e3zO1uWuVy4KEYttPIMF.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/one-piece">
                        <img style="" class="img-responsive shadow" title="Nonton anime: One Piece" src="https://image.tmdb.org/t/p/w154/zbu8sTetLWcX7oza1sddZkleSBG.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/radiant-2nd-season">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Radiant 2nd Season" src="https://image.tmdb.org/t/p/w154/yyYi2VphEvHedHKPzx3JA6HH5T7.jpg">
                        </a>
                        </div>

                        <div class="item slider-item">
                        <a href="/anime/fairy-tail-final-series">
                        <img style="" class="img-responsive shadow" title="Nonton anime: Fairy Tail: Final Series" src="https://image.tmdb.org/t/p/w154/acHknNCgR3ThjDE3S5aSN5xnRE1.jpg">
                        </a>
                        </div>
                    </div>
                </div>
            </header>
            <!-- TUTUP SLIDER -->

            <!-- Konten -->
            <div class="row" style="margin-bottom: 10px;">
                <div class="col" style="padding: 0; font-weight: bold;">
                    <nav class="navbar navbar-expand-lg" style="padding-left: 16px; background-color: #474747; border-bottom: 5px solid #2a2a2a; border-radius: 5px;">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarTogglerDemo04" style="line-height: 0;">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">A</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">B</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">C</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">D</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">E</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">F</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">G</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">H</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">I</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">J</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">K</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">L</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">M</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">N</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">O</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">P</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Q</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">R</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">S</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">T</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">U</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">V</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">W</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">X</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Y</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Z</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin-bottom: 15px;">
                <div style=" width:15%; margin-left:1%; margin-right:1%; background-color: #4698c0;">
                    1 of 3
                </div>
                <div style="width:58%; margin-right:1%;">
                    <!-- KONTEN -->
                    @yield('konten')
                </div>

                <div style="background-color:lime; width:24%;">
                    <div style="background-color: #1d447e; text-align: center; padding: 8px 0;">
                        <span style="color: white; font-weight: bold;"> CARI GENRE </span>
                    </div>

                    <div style="background-color: red; height: 95%; width: 100%;">
                        
                    </div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://nanime.tv/vendors/owlcarousel/owl.carousel.min.js?ver=0.17"></script>
        <script src="https://nanime.tv/template/nontonanime.org/js/ads.js?ver=0.17"></script>
        <script src="https://nanime.tv/template/nontonanime.org/js/script.js?ver=0.17"></script>

    </body>
</html>