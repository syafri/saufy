@extends('layouts.layout')
@section('konten')
    <div class="postbody">
        <div class="bixbox">
            <div class="releases">
                <h1><span>Daftar Manga</span></h1> 
                <!-- <span class="filter">Filter</span> -->
            </div>
            <div class="mrgn">
                <div class="nav_apb">
                    <span style="color: white">
                        @for($i=0; $i<=25; $i++)
                            <a href="#{{$abjad[$i]}}">{{$abjad[$i]}}</a>
                        @endfor                    

                    </span>
                </div>
                <div class="clear"></div>
                <!-- <div class="modex"> <a href="https://bacamanga.co/manga">Image Mode</a></div> -->
                <div class="soralist">
                    <div class="lxx"></div>
                    
                    @for($i=0; $i<=25; $i++)
                        <div class="blix"><span style="color: white;"><a name="{{$abjad[$i]}}">{{$abjad[$i]}}</a></span>
                            <ul>
                                @foreach($mangas[$i] as $mangaList)
                                    <li style="width: 50%; float: left;">
                                        <a class="series" rel="38481" href="{{url('manga/'.$mangaList->slug_manga)}}" style="color: white;">{{$mangaList->nama_manga}}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    @endfor

                </div>
            </div>
        </div>
    </div> 
@stop