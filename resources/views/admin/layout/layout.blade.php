<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Free Bootstrap Admin Template : Dream</title>

		<link href="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/css/bootstrap.css" rel="stylesheet" />
		<!-- <link href="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/css/font-awesome.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="{{ asset('/css/all.css') }}">
		
		<link href="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/css/custom-styles.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

		<!-- TABLE STYLES-->
		<link href="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	</head>
	<body>
	    <div id="wrapper">
			


	        <!-- NAV TOP  -->
	        @include('admin.topbar')

	        <!-- NAV SIDE  -->
	        @include('admin.sidebar')

	        <div id="page-wrapper" style="min-height: 644px;">
	            
	        	@yield('content')

	    	</div>

	    <script src="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/jquery-1.10.2.js"></script>

	    <script src="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/bootstrap.min.js"></script>

	    <script src="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/jquery.metisMenu.js"></script>

	    <script src="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/dataTables/jquery.dataTables.js"></script>
	    <script src="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/dataTables/dataTables.bootstrap.js"></script>
		<script>
			$(document).ready(function () {
				$('#dataTables-example').dataTable();
			});
		</script>
	    <script src="http://webthemez.com/demo/free-bootstrap-admin-template-dream/assets/js/custom-scripts.js"></script>
	    
	   </div>   
	</body>
</html>