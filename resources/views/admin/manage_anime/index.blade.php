@extends('admin.layout.layout')
@section('content')

	<div id="page-inner" style="min-height: 644px;">
	    <div class="row">
	        <div class="col-md-12">
	            <h1 class="page-header">
	                Tables Page <small>Responsive tables</small>
	            </h1>
	        </div>
	    </div> 
	     <!-- /. ROW  -->
	   
	    <div class="row">
	        <div class="col-md-12">
	            <!-- Advanced Tables -->
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                     Advanced Tables
	                </div>
	                <div class="panel-body">
	                    <div class="table-responsive">
	                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                            <thead>
	                                <tr>
	                                    <th>No</th>
	                                    <th>Manga</th>	                                    
	                                    <th>release</th>
	                                    <th>summary</th>
	                                    <th>Engine version</th>	                                    
	                                </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($manga as $manga)
		                                <tr class="odd gradeX">
		                                    <td>{{ $manga->id_manga }}</td>
		                                    <td>{{ $manga->nama_manga }}</td>
		                                    <td> <center> {{ $manga->release ? $manga->release : '-' }} </center></td>
		                                    <td> {{ $manga->summary ? $manga->summary : '-' }}</td>
		                                    <td>{{ $manga->nama_manga }}</td>
		                                </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	            <!--End Advanced Tables -->
	        </div>
	    </div>
	</div>
@stop