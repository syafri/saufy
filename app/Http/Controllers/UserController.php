<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(){
    	$manga = DB::table('manga')->paginate(12);
    	return view('home',['mangas' => $manga]);
    }

    public function mangaList(){
    	foreach (range('A', 'Z') as $char) {
		    $manga[] = DB::table('manga')->where('nama_manga', 'like', $char.'%')->get();
		    $abjad[] = $char;		    
		}
		return view('mangalist',['mangas' => $manga, 'abjad' => $abjad]);
    }

    public function chapterList($slug){
    	$chapter = DB::table('manga')->join('chapter', 'manga.id_manga', '=', 'chapter.id_manga')
    								 // ->select('episode_chapter', 'judul_chapter')
    								 ->where('slug_manga', '=', $slug)
    								 ->get();
    	
    // 	echo "<pre>";
  		// var_dump($chapter);
    // 	echo "</pre>";

    	return view('chapterlist',['chapter' => $chapter]);
    }
}