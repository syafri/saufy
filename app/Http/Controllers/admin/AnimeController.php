<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class AnimeController extends Controller
{

	public function __construct(){
        $this->middleware('auth');
    }

    
    public function index(){

    	// mengambil data dari table manga
    	$manga = DB::table('manga')->get();

    	return view('admin/manage_anime/index',['manga' => $manga]);
    }
    
    public function logout () {
	    auth()->logout();
	    return redirect('/');
	}
}
